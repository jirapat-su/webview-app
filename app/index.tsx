import { Platform } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import WebView, { WebViewMessageEvent } from "react-native-webview";
import { Text, View } from "../components/Themed";

export default function IndexPage() {
  const INJECTED_JAVASCRIPT = `(function() {
    window.ReactNativeWebView.postMessage(JSON.stringify(window.location));
})();`;

  const onMessage = (data: WebViewMessageEvent) => {
    console.log(data.nativeEvent.data);
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      {(() => {
        switch (Platform.OS) {
          case "ios":
          case "android":
            return (
              <WebView
                source={{ uri: "https://pi1hh.csb.app/" }}
                injectedJavaScript={INJECTED_JAVASCRIPT}
                onMessage={onMessage}
                startInLoadingState={true}
                renderLoading={() => <Text>Loading...</Text>}
                renderError={(errorName) => <Text>Error : {errorName}</Text>}
              />
            );
          default:
            return (
              <View style={{ flex: 1 }}>
                <Text>Hello World!</Text>
              </View>
            );
        }
      })()}
    </SafeAreaView>
  );
}
